import {all, call, put, takeLatest} from 'redux-saga/effects';
import axios from 'axios';
import { FETCH_DIRECTIONS_ERROR, FETCH_DIRECTIONS_REQUEST, FETCH_DIRECTIONS_SUCCESS } from '../actionType/types';


function* fetchDirectionsSaga(action) {
  // console.log('fetchDirectionsSaga', action);
  try {
    const {origin, destination} = action.payload;
    const mode = 'driving';
    const APIKEY = 'AIzaSyD9JlKfZL8qyovzAhPwJw7f7RdtWAUhpco'; // my presonal api
    const endPoint = `https://maps.googleapis.com/maps/api/directions/json?origin=${origin}&destination=${destination}&key=${APIKEY}&mode=${mode}`;

    const response = yield call(axios.get, endPoint);
    console.log('fetchDirectionsSaga response', response);
    if (response.data.routes.length > 0) {
      const points = response.data.routes[0].overview_polyline.points;
      const decodedPoints = decodePolyline(points);
      yield put({type: FETCH_DIRECTIONS_SUCCESS, payload: decodedPoints});
    }
  } catch (error) {
    yield put({type: FETCH_DIRECTIONS_ERROR, error});
  }
}

const decodePolyline = encoded => {
  const points = [];
  let index = 0;
  const len = encoded.length;
  let lat = 0;
  let lng = 0;

  while (index < len) {
    let b;
    let shift = 0;
    let result = 0;
    do {
      b = encoded.charCodeAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    const dlat = (result & 1) !== 0 ? ~(result >> 1) : result >> 1;
    lat += dlat;
    shift = 0;
    result = 0;
    do {
      b = encoded.charCodeAt(index++) - 63;
      result |= (b & 0x1f) << shift;
      shift += 5;
    } while (b >= 0x20);
    const dlng = (result & 1) !== 0 ? ~(result >> 1) : result >> 1;
    lng += dlng;
    points.push({latitude: lat / 1e5, longitude: lng / 1e5});
  }
  return points;
};

function* watchFetchDirections() {
  yield takeLatest(FETCH_DIRECTIONS_REQUEST, fetchDirectionsSaga);
}

export default function* rootSaga() {
  yield all([watchFetchDirections()]);
}
