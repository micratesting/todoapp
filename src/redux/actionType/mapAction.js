import { FETCH_DIRECTIONS_ERROR, FETCH_DIRECTIONS_REQUEST, FETCH_DIRECTIONS_SUCCESS } from "./types";

export const fetchDataRequest = () => ({
  type: FETCH_DIRECTIONS_REQUEST,
});

export const fetchDataSuccess = (data) => ({
  type: FETCH_DIRECTIONS_SUCCESS,
  payload: data,
});

export const fetchDataFailure = (error) => ({
  type: FETCH_DIRECTIONS_ERROR,
  payload: error,
});