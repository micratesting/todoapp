import { FETCH_DIRECTIONS_ERROR, FETCH_DIRECTIONS_REQUEST, FETCH_DIRECTIONS_SUCCESS } from "../actionType/types";

const initialState = {
  directions: null,
  loading: false,
  error: null,
};

// Define your mapReducer
const mapReducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_DIRECTIONS_REQUEST:
      return {
        ...state,
        loading: true,
        error: null,
      };
    case FETCH_DIRECTIONS_SUCCESS:
      return {
        ...state,
        directions: action.payload,
        loading: false,
      };
    case FETCH_DIRECTIONS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
};

export default mapReducer;
