import React from 'react';
import {View, Text, StyleSheet, Dimensions, Button} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import MapView, { Marker, Polyline} from 'react-native-maps';

const HomeScreen = () => {
  const origin = {latitude: 28.5703, longitude: 77.3218}; // Starting point
  const destination = {latitude: 28.6429, longitude: 77.2191}; // Ending point

  const dispatch = useDispatch();
  const directions = useSelector(state => state.map.directions);

  const handleGetDirections = () => {
    dispatch({
      type: 'FETCH_DIRECTIONS_REQUEST',
      payload: {
        origin: `${origin.latitude},${origin.longitude}`,
        destination: `${destination.latitude},${destination.longitude}`,
      },
    });
  };

//   console.log('directions', directions);

  return (
    <View style={styles.container}>
      <Text style={{fontSize: 15, textAlign: 'center'}}>
        {'To plot route click on below button'}
      </Text>
      <Button title="Get Directions" onPress={handleGetDirections} />

      {directions && (
        <MapView
          style={styles.maps}
          initialRegion={{
            latitude: (origin.latitude + destination.latitude) / 2,
            longitude: (origin.longitude + destination.longitude) / 2,
            latitudeDelta:
              Math.abs(origin.latitude - destination.latitude) * 2.5,
            longitudeDelta:
              Math.abs(origin.longitude - destination.longitude) * 2.5,
          }}>
          <Marker coordinate={origin} title="Origin" />
          <Marker coordinate={destination} title="Destination" />
          <Polyline
            coordinates={directions}
            strokeColor="blue" // Change this to customize the color of the route
            strokeWidth={5}
          />
        </MapView>
      )}
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  maps: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
  },
});

export default HomeScreen;
